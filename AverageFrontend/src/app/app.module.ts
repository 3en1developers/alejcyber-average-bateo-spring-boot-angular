import { BrowserModule } from '@angular/platform-browser';
import { Injectable, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HttpHandler, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {HomeComponent } from './components/home/home.component';

import {AppService} from './shared/services/app.service';
import {SharedModule} from './shared/shared.module';
import { CommonModule } from '@angular/common';
import { HomeModule } from './components/home/home.module';
import { InfoComponent } from './components/info/info.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home'},
  { path: 'home', component: HomeComponent},
  { path: 'info', component: InfoComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    InfoComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    CommonModule,
    HomeModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }