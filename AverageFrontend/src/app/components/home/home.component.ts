import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Average } from 'src/app/shared/models/average.model';
import { NivelDeAverage } from 'src/app/shared/models/nivel-de-average.model';
import { AppService } from '../../shared/services/app.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'Average';
  promedio: number;
  averages: Average[];
  niveles: NivelDeAverage[];


  constructor(private app: AppService) {
    
    this.app.jugadores().subscribe( res => {
      this.averages = res;
      console.log(res);
    },
    error => console.log(error));

    this.app.promedio().subscribe( res => {
      this.promedio = res;
      console.log(res);
    },
    error => console.log(error));
  
    this.app.niveles().subscribe( res => {
      this.niveles = res;
      console.log(res);
    },
    error => console.log(error));
  
  }

    

  ngOnInit(): void {
  }

}
