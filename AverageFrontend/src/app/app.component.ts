import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppService } from './shared/services/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
    constructor(private app: AppService, private http: HttpClient) {
    }

}