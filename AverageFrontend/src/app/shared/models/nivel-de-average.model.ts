import { Average } from "./average.model"

export class NivelDeAverage {
    public nivel: string;
    public average: Average;
}