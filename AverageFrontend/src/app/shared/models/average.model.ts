import { Jugador } from "./jugador.model";

export class Average {
    public hits?: number;
    public ab?: number;
    public avg?: number;
    public jugador?: Jugador;
}