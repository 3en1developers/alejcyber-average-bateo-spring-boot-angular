import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Average } from '../models/average.model';
import { NivelDeAverage } from '../models/nivel-de-average.model';


@Injectable()
export class AppService {

    API_ENDPOINT = environment.API_ENDPOINT;

    constructor(private http: HttpClient, private router: Router) {
    }

    jugadores() {
        return this.http.get<Average[]>(`${this.API_ENDPOINT}average/jugadores`)
    }

    promedio() {
        return this.http.get<number>(`${this.API_ENDPOINT}average/promedio`)
    }

    niveles() {
        return this.http.get<NivelDeAverage[]>(`${this.API_ENDPOINT}average/niveles`)
    }

}