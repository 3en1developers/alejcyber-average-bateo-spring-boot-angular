
const path = require('path');
const express = require('express');
const app = express();

// Serve static files
app.use(express.static(__dirname + '/dist/AppAverageFrontend'));

// Send all requests to index.html
app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname + '/dist/AppAverageFrontend/index.html'));
});

// default Heroku port
//app.listen(process.env.PORT || 5000);

const HOST = '0.0.0.0';
const PORT = process.env.PORT || 3000;
app.listen(HOST, PORT, () => {
    console.log(`Our app is running on port ${ PORT }`);
});