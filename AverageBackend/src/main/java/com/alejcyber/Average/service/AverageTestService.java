package com.alejcyber.Average.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alejcyber.Average.dto.Average;
import com.alejcyber.Average.dto.NivelDeAverage;
import com.alejcyber.Average.model.Jugador;
import com.alejcyber.Average.model.JugadorHasTemporada;
import com.alejcyber.Average.repository.IJugadorHasTemporadaRepository;
import com.alejcyber.Average.repository.IJugadorRepository;

@Service
public class AverageTestService {
	
	@Autowired
	private IJugadorRepository repoJugador;
	
	@Autowired
	private IJugadorHasTemporadaRepository repoTemporada;
	
	public void printResults() {
		
		int totalDeHitsColectivos = 0;
		int totalDeABColectivos = 0;
		
		List<Jugador> jugadores = repoJugador.findAll();
		List<Average> averages = new ArrayList<>();
		
		for(Jugador jugador : jugadores) {
			int hits = 0;
			int ab = 0;
			List<JugadorHasTemporada> temporadas = repoTemporada.findAllByJugadorId(jugador.getId());
			for(JugadorHasTemporada temporada : temporadas) {
				hits += temporada.getHits();
				ab += temporada.getAb();
				
				totalDeHitsColectivos += hits;
				totalDeABColectivos += ab;
			}
			
			Double avg = Double.valueOf(hits) / Double.valueOf(ab);
			Average average = new Average();
			
			average.setHits(hits);
			average.setAb(ab);
			average.setAvg(avg);
			average.setJugador(jugador);
			averages.add(average);
		}
		
		
		Double avgColectivo = Double.valueOf(totalDeHitsColectivos) / Double.valueOf(totalDeABColectivos);
				
		System.out.println("#################################################################");
		System.out.println("-- Averages por cada jugador :");
		
		String mensajePromedioPersonal = "%s tiene %d hits en %d turnos al bate, con un average de %.3f";
		for(Average average : averages){
			System.out.println(String.format(mensajePromedioPersonal, average.getJugador().getNombre(), average.getHits(), average.getAb(),average.getAvg()));
		}
		System.out.println("#################################################################");
		System.out.println(String.format("-- Promedio colectivo : %.3f", avgColectivo));
		System.out.println("#################################################################");

		String mensajePromedioMayor = "%s con un avg de %d es mayor al promedio colectivo %.3f";
		String mensajePromedioMenor = "%s con un avg de %d es menor al promedio colectivo %.3f";
		String mensajePromedioIgual = "%s con un avg de %d es igual al promedio colectivo %.3f";

		for(Average average : averages){

			if(average.getAvg() > avgColectivo) {
				System.out.println( String.format(mensajePromedioMayor, average.getJugador().getNombre(),average.getHits(), avgColectivo ));
			} else if(average.getAvg() < avgColectivo) {
				System.out.println( String.format(mensajePromedioMenor, average.getJugador().getNombre(),average.getHits(), avgColectivo ));
			} else if(average.getAvg() == avgColectivo) {
				System.out.println( String.format(mensajePromedioIgual, average.getJugador().getNombre(),average.getHits(), avgColectivo ));
			}
		}
		
		System.out.println("#################################################################");
		
	}

	public List<Average> obtenerAveragesPorJugador(){
		
		List<Jugador> jugadores = repoJugador.findAll();
		List<Average> averages = new ArrayList<>();
		
		for(Jugador jugador : jugadores) {
			int hits = 0;
			int ab = 0;
			List<JugadorHasTemporada> temporadas = repoTemporada.findAllByJugadorId(jugador.getId());
			for(JugadorHasTemporada temporada : temporadas) {
				hits += temporada.getHits();
				ab += temporada.getAb();
			}
			
			Double avg = Double.valueOf(hits) / Double.valueOf(ab);
			Average average = new Average();
			
			average.setHits(hits);
			average.setAb(ab);
			average.setAvg(avg);
			average.setJugador(jugador);
			averages.add(average);
		}
		
		return averages;
	}

	public Double obtenerAveragePromedio(){
		
		int totalDeHitsColectivos = 0;
		int totalDeABColectivos = 0;
		
		List<Jugador> jugadores = repoJugador.findAll();
		
		for(Jugador jugador : jugadores) {
			int hits = 0;
			int ab = 0;
			List<JugadorHasTemporada> temporadas = repoTemporada.findAllByJugadorId(jugador.getId());
			for(JugadorHasTemporada temporada : temporadas) {
				hits += temporada.getHits();
				ab += temporada.getAb();
				
				totalDeHitsColectivos += hits;
				totalDeABColectivos += ab;
			}
		}
		
		return Double.valueOf(totalDeHitsColectivos) / Double.valueOf(totalDeABColectivos);

	}

	public List<NivelDeAverage> obtenerNivelDeAveragePorJugador(){
		int totalDeHitsColectivos = 0;
		int totalDeABColectivos = 0;
		
		List<Jugador> jugadores = repoJugador.findAll();
		List<Average> averages = new ArrayList<>();
		List<NivelDeAverage> niveles = new ArrayList<>();

		for(Jugador jugador : jugadores) {
			int hits = 0;
			int ab = 0;
			List<JugadorHasTemporada> temporadas = repoTemporada.findAllByJugadorId(jugador.getId());
			for(JugadorHasTemporada temporada : temporadas) {
				hits += temporada.getHits();
				ab += temporada.getAb();
				
				totalDeHitsColectivos += hits;
				totalDeABColectivos += ab;
			}
			
			Double avg = Double.valueOf(hits) / Double.valueOf(ab);
			Average average = new Average();
			
			average.setHits(hits);
			average.setAb(ab);
			average.setAvg(avg);
			average.setJugador(jugador);
			averages.add(average);
		}
				
		Double avgColectivo = Double.valueOf(totalDeHitsColectivos) / Double.valueOf(totalDeABColectivos);

		for(Average average : averages){

			NivelDeAverage nivel = new NivelDeAverage();
			nivel.setAverage(average);

			if(average.getAvg() > avgColectivo) {
				nivel.setNivel("mayor");
			} else if(average.getAvg() < avgColectivo) {
				nivel.setNivel("menor");
			} else if(average.getAvg() == avgColectivo) {
				nivel.setNivel("igual");
			}
			niveles.add(nivel);
		}
		
		return niveles;	
	}

}
