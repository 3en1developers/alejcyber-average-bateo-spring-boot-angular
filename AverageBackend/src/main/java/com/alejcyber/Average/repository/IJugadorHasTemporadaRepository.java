package com.alejcyber.Average.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alejcyber.Average.model.JugadorHasTemporada;

@Repository
public interface IJugadorHasTemporadaRepository extends JpaRepository<JugadorHasTemporada, Integer> {
	
    List<JugadorHasTemporada> findAllByJugadorId(Integer id);
}
