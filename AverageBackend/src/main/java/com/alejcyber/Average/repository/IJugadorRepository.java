package com.alejcyber.Average.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alejcyber.Average.model.Jugador;

@Repository
public interface IJugadorRepository extends JpaRepository<Jugador, Integer> {

}
