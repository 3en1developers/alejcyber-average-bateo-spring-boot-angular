package com.alejcyber.Average.dto;

public class NivelDeAverage {
    private Average average;
    private String nivel;


    public Average getAverage() {
        return average;
    }

    public void setAverage(Average average) {
        this.average = average;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

}
