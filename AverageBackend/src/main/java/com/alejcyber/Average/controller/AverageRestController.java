package com.alejcyber.Average.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestController;

import com.alejcyber.Average.dto.Average;
import com.alejcyber.Average.dto.NivelDeAverage;
import com.alejcyber.Average.service.AverageTestService;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("average")
@CrossOrigin(origins="*")
public class AverageRestController {
	
	@Autowired
	private AverageTestService service;

	@RequestMapping("/user")
	public Principal user(Principal user) {
	  return user;
	}
	
	@GetMapping("/jugadores")
	public List<Average> averagesPorJugador() {	
		return service.obtenerAveragesPorJugador();
	}

	@GetMapping("/promedio")
	public Double averagePromedio() {	
		return service.obtenerAveragePromedio();
	}

	@GetMapping("/niveles")
	public List<NivelDeAverage> nivelDeAveragePorJugador() {	
		return service.obtenerNivelDeAveragePorJugador();
	}
}
