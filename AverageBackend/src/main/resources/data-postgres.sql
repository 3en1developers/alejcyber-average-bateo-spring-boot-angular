/*
    Password generadas usando BCrypt
    - 12 Rounds
    https://bcrypt-generator.com/
    
    username: averageadmin, password: 1000
    username: av, password: 1
*/
INSERT INTO usuario(nombre, clave) VALUES('averageadmin', 
'$2y$12$ku2/JYSX2Lmrogx1WSCHVuqiExPjPpSO7KwGumheyqUMdWHapmSRS');
INSERT INTO usuario(nombre, clave) VALUES('av', 
'$2y$12$kcznLfhozGiExcistGGjXumEK5.KU.rJbcIF7eGOVOJGTWSu3IDty');

INSERT INTO equipo(id, nombre) VALUES(1,'Magallanes');
INSERT INTO equipo(id, nombre) VALUES(2,'Leones');
INSERT INTO equipo(id, nombre) VALUES(3, 'Tigres');

-- Magallanes
INSERT INTO jugador(id, nombre, equipo_id) VALUES(1,'Juan', 1);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(2,'Pedro', 1);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(3,'Alejandro', 1);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(4,'Damian', 1);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(5,'Julian', 1);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(6,'Yona', 1);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(7,'Victor', 1);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(8,'Miguel', 1);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(9,'Zeal', 1);

-- Leones
INSERT INTO jugador(id, nombre, equipo_id) VALUES(10,'Ruben', 2);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(11,'Luis', 2);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(12,'Jonathan', 2);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(13,'Daniel', 2);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(14,'Estiven', 2);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(15,'Danry', 2);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(16,'Victor', 2);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(17,'Ali', 2);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(18,'William', 2);

-- Tigres
INSERT INTO jugador(id, nombre, equipo_id) VALUES(19,'Angel', 3);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(20,'Jhony', 3);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(21,'Ronny', 3);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(22,'Heiker', 3);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(23,'Eguy', 3);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(24,'Tomas', 3);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(25,'Cade', 3);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(26,'Niuman', 3);
INSERT INTO jugador(id, nombre, equipo_id) VALUES(27,'Carlos', 3);

-- Temporada 2020 - Magallanes
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(1,300, 20, 'LVBP', 2020, 1);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(2,400, 60, 'LVBP', 2020, 2);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(3,370, 150, 'LVBP', 2020, 3);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(4,330, 20, 'LVBP', 2020, 4);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(5,400, 300, 'LVBP', 2020, 5);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(6,370, 350, 'LVBP', 2020, 6);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(7,320, 50, 'LVBP', 2020, 7);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(8,350, 345, 'LVBP', 2020, 8);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(9,410, 399, 'LVBP', 2020, 9);

-- Temporada 2020 - Leones
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(10,300, 22, 'LVBP', 2020, 10);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(11,400, 57, 'LVBP', 2020, 11);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(12,370, 158, 'LVBP', 2020, 12);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(13,330, 2, 'LVBP', 2020, 13);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(14,400, 89, 'LVBP', 2020, 14);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(15,370, 123, 'LVBP', 2020, 15);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(16,320, 87, 'LVBP', 2020, 16);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(17,350, 387, 'LVBP', 2020, 17);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(18,410, 299, 'LVBP', 2020, 18);

-- Temporada 2020 - Tigres
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(19,100, 67, 'LVBP', 2020, 19);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(20,200, 89, 'LVBP', 2020, 20);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(21,370, 201, 'LVBP', 2020, 21);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(22,130, 22, 'LVBP', 2020, 22);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(23,200, 19, 'LVBP', 2020, 23);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(24,470, 323, 'LVBP', 2020, 24);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(25,220, 187, 'LVBP', 2020, 25);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(26,350, 287, 'LVBP', 2020, 26);
INSERT INTO jugador_has_temporada(id, ab, hits, liga, temporada, jugador_id) VALUES(27,310, 199, 'LVBP', 2020, 27);


