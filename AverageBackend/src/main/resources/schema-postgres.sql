DROP TABLE IF EXISTS "jugador_has_temporada";
DROP TABLE IF EXISTS "jugador";
DROP TABLE IF EXISTS "equipo";
DROP TABLE IF EXISTS "usuario";

CREATE TABLE "jugador"(
    "id" serial PRIMARY KEY,
    "nombre" VARCHAR(255) NOT NULL,
    "equipo_id" INTEGER NOT NULL
);

CREATE TABLE "jugador_has_temporada"(
	"id" serial PRIMARY KEY,    
	"ab" INTEGER NOT NULL,
    "hits" INTEGER NOT NULL,
    "liga" VARCHAR(255) NOT NULL,
    "temporada" INTEGER NOT NULL,
    "jugador_id" INTEGER NOT NULL
);


CREATE TABLE "equipo"(
	"id" serial PRIMARY KEY,    
	"nombre" VARCHAR(255) NOT NULL
);

CREATE TABLE "usuario"(
	"id" serial PRIMARY KEY,    
    "nombre" VARCHAR(255) NOT NULL,
    "clave" VARCHAR(255) NOT NULL
);

ALTER TABLE
    "jugador_has_temporada" ADD CONSTRAINT "jugador_has_temporada_liga_foreign" FOREIGN KEY("jugador_id") REFERENCES "jugador"("id");
ALTER TABLE
    "jugador" ADD CONSTRAINT "jugador_equipo_foreign" FOREIGN KEY("equipo_id") REFERENCES "equipo"("id");
    
    
    