# Average Bateo Spring Boot - Angular

Reto 1: cumplido por Alejandro Pedrique

## Requerimientos

1. Obtener el average de cada Jugador
2. Obtener el promedio
3. Mostrar el nivel de cada jugador con respecto al promedio(menor, mayor, igual) 

## Solucion


Mi solucion para este reto es usar una base datos PostgreSQL, un servicio REST hecho en Spring Boot y una aplicacion web hecha en Angular y desplegarlos en Heroku


## Como correr el proyecto

Para ejecutar el proyecto necesitas tener instalado Node.js Y Java JDK 8

## Instalar en JDK de Java

Dirigirse a este link y descargar la version para tu sistema operativo

https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html

## Instalar Node.js

https://nodejs.org/es/download/

## Instalar PostgreSQL

https://www.postgresql.org/download/

Una vez descargado abrir una terminal y ejecutar los siguientes comandos

- Conectar con postgre

>sudo -u postgres psql

- crear base de datos

> create database average;

- crear usuario

> create user averageadmin with encrypted password '1000';

- otorgar privilegios sobre la base de datos

>grant all privileges on database average to averageadmin;

- otorgar privilegios de superusuario

>alter user averageadmin with superuser;


## Desplegar en local

Desde la terminal ubicarse en el siguiente directorio 

- Backend Java
>cd ${tu_ruta_personal}/Average/AverageBackend

- En Linux
>./mvnw
- En Windows 
>mvnw

- Frontend Angular
>cd ${tu_ruta_personal}/Average/AverageFrontend
>ng serve

- Acceder a esta ruta http://localhost:4200

A continuacion podras observar las tablas de average **Home** y la pagina de informacion basica del proyecto **Info**

## Postman: probar los endpoints

Sino quieres consumir los servicios desde la aplicacion de Angular puedes solo usar Postman, este proyecto incluye una coleccion de Postman para que puedas importarla y probar todos los endpoints, en esta coleccion estan tanto los endpoints en local como los desplegados en heroku

>3en1developers-average.postman_collection.json

##En la nube: Despliegue en Heroku

Aqui te dejo los links del backend y el frontend desplegados en Heroku, el link del backend lo puedes usar para probar los endpoints

>https://alejcyber-spring-boot-average.herokuapp.com/average/jugadores

>https://alejcyber-spring-boot-average.herokuapp.com/average/promedio

>https://alejcyber-spring-boot-average.herokuapp.com/average/niveles

O sino puedes solo probar como se ve el frontend desde tu navegador

>https://alejcyber-average-frontend.herokuapp.com
